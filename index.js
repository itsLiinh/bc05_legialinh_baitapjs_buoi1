/**
 * Bài 1
 *
 * Input: tiền lương 1 ngày (amountSalary) , số ngày làm (workedDay)
 *
 * Step1: Tạo biến amountSalary và  workedDay
 * Step2: Gắn giá trị cho amountSalary và workedDay
 * Step3: Tạo biến cho workerSalary và gắn công thức workedDay * amountSalary (lương 1 ngày * số ngày làm)
 * Step4: In kết quả workerSalary ra console
 *
 * Output: Lương nhân viên (workerSalary)
 */

var amountSalary, workedDay;

amountSalary = 100000;
workedDay = 15;

var workerSalary = workedDay * amountSalary;
console.log("workerSalary: ", workerSalary);

/**
 * Bài 2
 *
 * Input: 5 số thực
 *
 * Step1: Tạo biến cho 5 số ( num1, num2, num3, num4, num5)
 * Step2: Gắn cho mỗi số 1 giá trị vd : num1= number
 * Step3: Tạo biến cho giá trị trung bình và gắn công thức ( tổng của 5 số cộng lại) chia 5
 * Step4: In kết quả giá trị trung bình ra console
 *
 * Output: Giá trị trung bình
 */

var num1, num2, num3, num4, num5;

num1 = 3;
num2 = 4;
num3 = 6;
num4 = 8;
num5 = 10;

var num6 = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("num6: ", num6);

/**
 * Bài 3
 *
 * Input: giá trị tiền USD (valueUSD), số tiền cần đổi (totalUSD)
 *
 * Step1: Tạo biến valueUSD và totalUSA
 * Step2: Gắn giá trị cho valueUSD và totalUSD bằng number
 * Step3: tạo biến cho totalVND và gắn công thức valueUSD * totalUSD
 * Step4: In kết quả totalVND ra console
 *
 * Output: số tiền quy ra VND (totalVND)
 */

var valueUSD, totalUSD;

valueUSD = 23500;
totalUSD = 3;

var totalVND = valueUSD * totalUSD;

console.log("totalVND: ", totalVND);

/**
 * Bài 4
 *
 * Input: chiều dài (a), chiều rộng (b)
 *
 * Step1: Tạo biến cho chiều dài và chiều rộng
 * Step2: Gắn giá trị cho a và b bằng number
 * Step3: Tạo biến cho dienTichHcn và gắn công thức a * b ~ dienTichHcn=( chiều dài * chiều rộng)
 * Step4: In công thức dienTichHcn ra console
 * Step5: Tạo biến cho chuVihHcn và gắn công thức (a + b)*2 ~ chuViHcn=(dài + rộng)*2
 * Step6: In công thức chuViHcn ra console
 *
 * Output: diện tích HCN vs chu vi HCN
 *
 */

var a, b;

a = 4;
b = 2;

var dienTichHcn = a * b;
console.log(" dienTichHcn: ", dienTichHcn);

var chuViHcn = (a + b) * 2;
console.log("chuViHcn: ", chuViHcn);

/**
  * Bài 5
  *
  * Input: a,b,c
  * 
  * Step1: Tạo biến a ( 1 số có 2 chữ số) 
  * 
  * Step2: gắn giá trị cho a bằng number
  * gắn giá trị cho b bằng công thức tách hàng chục Math.floor (a/10)
  * gắn giá trị cho c bằng công thức tách hàng đơn vị Math.floor (a%10)
  * Step3: Tạo biến sum và gắn công thức b+c 
  * Step4: In kết quả sum ra console

  * Output: tổng 2 ký số a+b (sum)
  */

var a, b, c;

a = 25;
b = Math.floor(a / 10);
c = Math.floor(a % 10);

var sum = b + c;
console.log("sum: ", sum);
